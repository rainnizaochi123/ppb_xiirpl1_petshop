import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:healthypet/models/doctor_model.dart';
import 'package:healthypet/models/service_model.dart';

var selectedService = 0;
var menus = [
  FeatherIcons.home,
  FeatherIcons.heart,
  FeatherIcons.messageCircle,
  FeatherIcons.user
];
var selectedMenu = 0;

class HomeScreen extends StatelessWidget {
  HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);

    return Scaffold(
      extendBody: true,
      bottomNavigationBar: _bottomNavigationBar(),
      body: Container(
        // Change the color below
        color: Color.fromARGB(255, 191, 190, 190), // Change this to your desired color
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                _greetings(),
                const SizedBox(
                  height: 16,
                ),
                _card(),
                const SizedBox(
                  height: 20,
                ),
                _search(),
                const SizedBox(
                  height: 20,
                ),
                _services(),
                const SizedBox(
                  height: 27,
                ),
                _doctors(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  BottomNavigationBar _bottomNavigationBar() => BottomNavigationBar(
        selectedItemColor: Colors.black,
        type: BottomNavigationBarType.fixed,
        items: menus
            .map((e) =>
                BottomNavigationBarItem(icon: Icon(e), label: e.toString()))
            .toList(),
        showSelectedLabels: false,
        showUnselectedLabels: false,
        unselectedItemColor: const Color(0xFFBFBFBF),
        backgroundColor: Color.fromARGB(255, 43, 43, 43),
      );

  ListView _doctors() {
    return ListView.separated(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (context, index) => _doctor(doctors[index]),
        separatorBuilder: (context, index) => const SizedBox(
              height: 11,
            ),
        itemCount: doctors.length);
  }

  Container _doctor(DoctorModel doctorModel) {
    return Container(
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
          borderRadius: BorderRadius.circular(14),
          boxShadow: [
            BoxShadow(
                color: const Color(0xFF35385A).withOpacity(.12),
                blurRadius: 30,
                offset: const Offset(0, 2))
          ]),
      child: Row(children: [
        Container(
          clipBehavior: Clip.hardEdge,
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
          child: Image.asset(
            'assets/images/${doctorModel.image}',
            width: 100,
            height: 103,
          ),
        ),
        const SizedBox(
          width: 20,
        ),
        Flexible(
          fit: FlexFit.tight,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                doctorModel.name,
                style: GoogleFonts.manrope(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: const Color(0xFF3F3E3F)),
              ),
              const SizedBox(
                height: 7,
              ),
              RichText(
                  text: TextSpan(
                      text: "Service: ${doctorModel.services.join(', ')}",
                      style: GoogleFonts.manrope(
                          fontSize: 12, color: Colors.black))),
              const SizedBox(
                height: 7,
              ),
              Row(
                children: [
                  const Icon(
                    FeatherIcons.mapPin,
                    size: 14,
                    color: Color(0xFFACA3A3),
                  ),
                  const SizedBox(
                    width: 7,
                  ),
                  Text("${doctorModel.distance}km",
                      style: GoogleFonts.manrope(
                        fontSize: 12,
                        color: const Color(0xFFACA3A3),
                      ))
                ],
              ),
              const SizedBox(
                height: 7,
              ),
              Row(
                children: [
                  Text(
                    "Available for",
                    style: GoogleFonts.manrope(
                        color: const Color(0xFF50CC98),
                        fontWeight: FontWeight.bold,
                        fontSize: 12),
                  ),
                  const Spacer(),
                  SvgPicture.asset('assets/svgs/cat.svg'),
                  const SizedBox(
                    width: 10,
                  ),
                  SvgPicture.asset('assets/svgs/dog.svg'),
                ],
              ),
            ],
          ),
        )
      ]),
    );
  }

  SizedBox _services() {
    return SizedBox(
      height: 36,
      child: ListView.separated(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) => Container(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                    color: selectedService == index
                        ? Color.fromARGB(255, 191, 190, 190)
                        : Color.fromARGB(255, 191, 190, 190),
                    border: selectedService == index
                        ? Border.all(
                            color: Color.fromARGB(255, 191, 190, 190).withOpacity(.22),
                            width: 0)
                        : null,
                    borderRadius: BorderRadius.circular(10)),
                child: Center(
                    child: Text(
                  Service.all()[index],
                  style: GoogleFonts.manrope(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: selectedService == index
                          ? Colors.black
                          : Colors.black.withOpacity(0.4)),
                )),
              ),
          separatorBuilder: (context, index) => const SizedBox(
                width: 10,
              ),
          itemCount: Service.all().length),
    );
  }

Widget _search() {
  return Builder(
    builder: (context) {
      double screenWidth = MediaQuery.of(context).size.width;
      double searchWidth = screenWidth * 0.8; // Set to 50% of the screen width

      return Row(
        children: [
          Container(
            margin: const EdgeInsets.only(left: 20),
            width: searchWidth,
            child: TextFormField(
              decoration: InputDecoration(
                contentPadding:
                    const EdgeInsets.symmetric(vertical: 4, horizontal: 4),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(14),
                  borderSide: BorderSide.none,
                ),
                prefixIcon: const Icon(
                  FeatherIcons.search,
                  color: Colors.black,
                ),
                hintText: "Search your lovely cat",
                hintStyle: GoogleFonts.manrope(
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  height: 150 / 100,
                ),
                filled: true,
                fillColor: Color.fromARGB(255, 191, 190, 190),
              ),
            ),
          ),
          IconButton(
            onPressed: () {
              // Add your hamburger icon tap logic here
            },
            icon: const Icon(
              Icons.menu, // Replace with your hamburger icon
              color: Colors.black, // Change the color as needed
            ),
          ),
        ],
      );
    },
  );
}



  AspectRatio _card() {
    return AspectRatio(
      aspectRatio: 336 / 184,
      child: Container(
        clipBehavior: Clip.hardEdge,
        margin: const EdgeInsets.symmetric(horizontal: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14),
          color: Color.fromARGB(255, 43, 43, 43),
        ),
        child: Stack(
        children: [
          Positioned(
            left: 220,
            top:  -100,
            child: Image.asset(
              'assets/images/donate.jpg',
              height: 500,
              width: 230,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 22),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                    text: TextSpan(
                        text: "    Donate",
                        style: GoogleFonts.manrope(
                            fontSize: 30,
                            color: Colors.white,
                            fontWeight: FontWeight.w800,
                            height: 150 / 100),
                            
                        children: const [
                      TextSpan(
                          text: "\nhere for cat",
                          style: TextStyle(
                            fontSize: 30,
                              color: Colors.white,
                              fontWeight: FontWeight.w800)),
                      TextSpan(text: "\n care needs"),
                      
                    ])),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  padding: const EdgeInsets.only(top: 1, left: 10),
                  margin: const EdgeInsets.only(left: 5), // Add margin to the left
                  width: 125,
                  height: 40,
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(1.0),
                      border: Border.all(
                          color: Colors.white.withOpacity(.10), width: 1),
                      borderRadius: BorderRadius.circular(15)),
                  child: Text(
                    " Donate",
                    style: GoogleFonts.manrope(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                )
              ],
            ),
          )
        ]),
      ),
    );
  }

Padding _greetings() {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'Karawang, Indonesia',
          style: GoogleFonts.manrope(
            fontSize: 24,
            fontWeight: FontWeight.w800,
            color: const Color(0xFF3F3E3F),
          ),
        ),
        Row(
          children: [
            IconButton(
              onPressed: () {},
              icon: const Icon(
                FeatherIcons.bell,
                color: Colors.black,
              ),
            ),
            // Add your new icon here
            IconButton(
              onPressed: () {
                // Handle the new icon's tap event
              },
              icon: const Icon(
                Icons.message, // Replace with the actual icon you want to use
                color: Colors.black, // Change the color as needed
              ),
            ),
          ],
        ),
      ],
    ),
  );
}
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:healthypet/screens/awal_screen.dart'; // Import your awal_screen.dart file

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

    Future.delayed(const Duration(seconds: 5)).then((value) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
            builder: (context) => MyApp(), // Change to AwalScreen
          ),
          (route) => false);
    });

return Scaffold(
  body: Stack(
    children: [
      Positioned.fill(
        child: Image.asset(
          'assets/images/awal.jpg',
          fit: BoxFit.cover,
        ),
      ),
      Center(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 100), // Add some padding at the top
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: "Helping you\nto keep ",
                  style: GoogleFonts.manrope(
                    fontSize: 24,
                    color: Color.fromARGB(255, 255, 255, 255),
                    letterSpacing: 3.5 / 100,
                    height: 152 / 100,
                  ),
                  children: const [
                    TextSpan(
                      text: "your bestie",
                      style: TextStyle(
                        color: Color.fromARGB(255, 255, 255, 255),
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    TextSpan(text: "\nstay healthy!"),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    ],
  ),
);
  }}
import 'package:healthypet/models/service_model.dart';

class DoctorModel {
  String name;
  String image;
  List<String> services;
  int distance;

  DoctorModel(
      {required this.name,
      required this.image,
      required this.services,
      required this.distance});
}

var doctors = [
  DoctorModel(
      name: "Anggora Kece ",
      image: "kucing.jpg",
      services: [Service.nearby,],
      distance: 10),
  DoctorModel(
      name: "Whiskas ",
      image: "makanan.jpg",
      services: [Service.nearby,],
      distance: 10),
];
